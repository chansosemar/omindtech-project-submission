import {combineReducers} from 'redux';
import auth from './authReducer';
import grup from './addGroupReducer';

export default combineReducers({
  auth,
  grup
});
