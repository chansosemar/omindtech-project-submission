import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Text, View, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import HomePage from '../screens/HomePage';
import LivePage from '../screens/LivePage';
import DiskusiPage from '../screens/DiskusiPage';
import ProfilPage from '../screens/ProfilPage';
import GroupPage from '../screens/GroupPage';

const Stack = createStackNavigator();

export const Home = (props) => {
	const navigation = useNavigation();
	return (
		<Stack.Navigator>
			<Stack.Screen
				name="Home"
				component={HomePage}
				options={{
					headerStyle: {
						backgroundColor: '#1f99d3',
						height: 80,
					},
					headerTintColor: '#fff',
					headerTitleStyle: {
						fontSize: 18,
					},
				}}
			/>
		</Stack.Navigator>
	);
};

export const Live = (props) => {
	const navigation = useNavigation();
	return (
		<Stack.Navigator>
			<Stack.Screen
				name="Live"
				component={LivePage}
				options={{
					headerStyle: {
						backgroundColor: '#1f99d3',
						height: 80,
					},
					headerTintColor: '#fff',
					headerTitleStyle: {
						fontSize: 18,
					},
				}}
			/>
		</Stack.Navigator>
	);
};

export const Diskusi = (props) => {
	const navigation = useNavigation();
	return (
		<Stack.Navigator>
			<Stack.Screen
				name="Diskusi"
				component={DiskusiPage}
				options={{
					headerStyle: {
						backgroundColor: '#1f99d3',
						height: 80,
					},
					headerTintColor: '#fff',
					headerTitleStyle: {
						fontSize: 18,
					},
				}}
			/>
			<Stack.Screen
				name="Buat Grup"
				component={GroupPage}
				options={{
					headerStyle: {
						backgroundColor: '#1f99d3',
						height: 80,
					},
					headerTintColor: '#fff',
					headerTitleStyle: {
						fontSize: 18,
					},
				}}
			/>
		</Stack.Navigator>
	);
};

export const Profil = (props) => {
	const navigation = useNavigation();
	return (
		<Stack.Navigator>
			<Stack.Screen
				name="Profil"
				component={ProfilPage}
				options={{
					headerStyle: {
						backgroundColor: '#1f99d3',
						height: 80,
					},
					headerTintColor: '#fff',
					headerTitleStyle: {
						fontSize: 18,
					},
				}}
			/>
		</Stack.Navigator>
	);
};
