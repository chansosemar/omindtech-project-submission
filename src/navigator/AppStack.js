import React, {useEffect, useState} from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {
	createStackNavigator,
	TransitionPresets,
	CardStyleInterpolators,
} from '@react-navigation/stack';
import {connect} from 'react-redux';
import MainNavigator from './MainNavigator';
import Login from '../screens/LoginPage';

const Stack = createStackNavigator();

const AppStack = (props) => {
	return (
		<NavigationContainer>
			<Stack.Navigator>
				{props.statusLogin ? (
					<Stack.Screen
						name="Main"
						component={MainNavigator}
						options={{headerShown: false}}
					/>
				) : (
					<Stack.Screen name="Login" component={Login} options={{headerShown: false}} />
				)}
			</Stack.Navigator>
		</NavigationContainer>
	);
};

const mapStateToProps = (state) => ({
	statusLogin: state.auth.isLoggedIn,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(AppStack);
