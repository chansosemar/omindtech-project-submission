import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import * as GLOBAL from '../constant';
import {Text} from 'react-native';

import {Home, Live, Diskusi, Profil} from './StackScreen';

const Tab = createBottomTabNavigator();

const MainNavigator = () => {
	return (
			<Tab.Navigator
		      initialRouteName={'Homepage'}
		      tabBarOptions={{
		        activeTintColor: '#1f99d3',
		        inactiveTintColor: '#222',
		        style: {
		          backgroundColor: '#fff',
		          height: 60,
		          width: GLOBAL.WDIMENS,
		          borderTopWidth:0
		        },
		        tabStyle: {
		          paddingVertical: 10,
		        },
		      }}
		      screenOptions={({route}) => ({
		        tabBarLabel: ({focused, color, size}) => {
		          return (
		            <Text
		              style={{fontSize: 10, color}}>
		              {' '}
		              {route.name}{' '}
		            </Text>
		          );
		        },
		        tabBarIcon: ({focused, color, size}) => {
		          let iconName;

		          if (route.name === 'Home') {
		            iconName = 'home-outline';
		          } else if (route.name === 'Live') {
		            iconName = 'play-outline';
		          } else if (route.name === 'Diskusi') {
		            iconName = 'chatbubbles-outline';
		          } else if (route.name === 'Profil') {
		            iconName = 'person-outline';
		          }
		          return <Icon name={iconName} size={25} color={color} />;
		        },
		      })}>
		      <Tab.Screen name="Home" component={Home} />
		      <Tab.Screen name="Live" component={Live} />
		      <Tab.Screen name="Diskusi" component={Diskusi} />
		      <Tab.Screen name="Profil" component={Profil} />
		    </Tab.Navigator>
		)

}

export default MainNavigator;