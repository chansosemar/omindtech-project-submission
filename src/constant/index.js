
// DIMENSIONS
import {Dimensions} from 'react-native';
export const HDIMENS = Dimensions.get('window').height
export const WDIMENS = Dimensions.get('window').width


// CONTAINER
export const CONTAINER = {
		height: HDIMENS,
		backgroundColor:'#fff',
		alignItems:'center'
	}

export const CONTENT = {
	width: WDIMENS * 0.8,
}


// BUTTON
export const MAINBTN = {
	backgroundColor:'#1f99d3',
	width:WDIMENS * 0.8,
	alignItems:'center',
	justifyContent:'center',
	height:50,
	borderRadius:5
}

export const TXTMAINBTN = {
	color:'#fff',
	fontSize:18,
	fontWeight:'bold'
}


// FORM INPUT
export const FORMINPUT = {
  width: WDIMENS * 0.8,
  backgroundColor: '#545454',
  borderRadius: 5,
  color: '#fff',
  marginVertical: 5,
  padding: 10,
};

export const FORMSNDINPUT = {
  width: WDIMENS * 0.8,
  backgroundColor: '#545454',
  borderRadius: 5,
  color: '#fff',
  marginVertical: 5,
  padding: 10,
};