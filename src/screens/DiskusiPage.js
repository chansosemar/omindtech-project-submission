import React from 'react';
import {
	View,
	Text,
	Image,
	StyleSheet,
	TouchableOpacity,
	Dimensions,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {connect} from 'react-redux';
import * as GLOBAL from '../constant';

function DiskusiPage(props) {
	const navigation = useNavigation();
	return (
		<View style={GLOBAL.CONTAINER}>
			<View style={[GLOBAL.CONTENT, styles.position]}>
				{props.grup == null ? (
					<Text style={styles.groupText}>Belum Ada Grup</Text>
				) : (
				<>
					
					<Text style={styles.groupText}>{props.grup.nama}</Text>
					<Text style={styles.groupText}>Anggota Group : {props.grup.anggota_grup}</Text>

				</>
				)}
				<TouchableOpacity
					style={GLOBAL.MAINBTN}
					onPress={() => navigation.navigate('Buat Grup')}>
					<Text style={GLOBAL.TXTMAINBTN}>Buat Grup</Text>
				</TouchableOpacity>
			</View>
		</View>
	);
}

const mapStateToProps = (state) => ({
	grup: state.grup.data.data,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(DiskusiPage);

const styles = StyleSheet.create({
	position: {
		position: 'absolute',
		top: GLOBAL.HDIMENS * 0.1,
	},
	groupText: {
		fontSize: 20,
		alignSelf: 'center',
		marginVertical: 10,
	},
});
