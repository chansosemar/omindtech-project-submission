import React, {useState} from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity, TextInput} from 'react-native';
import {connect} from 'react-redux';
import * as GLOBAL from '../constant'

function LoginPage(props){
	const [email, setEmail] = useState('tentor@gmail.com')
	const [password, setPassword] = useState('123123')


	const submit = () => {
      props.processLogin({email, password});
  };

	return (
		<View style={GLOBAL.CONTAINER}>
			<View style={[GLOBAL.CONTENT, styles.position]}>
				<Text style={{alignSelf:'center', marginVertical:20, fontSize: 20}}>Hello, Selamat Datang</Text>
				<TextInput
		          value={email}
		          onChangeText={(text) => setEmail(text)}
		          style={GLOBAL.FORMINPUT}
		        />
		        <TextInput
		          value={password}
		          onChangeText={(text) => setPassword(text)}
		          style={GLOBAL.FORMINPUT}
		          secureTextEntry
		        />

				<TouchableOpacity
					onPress={() => submit()}
					style={GLOBAL.MAINBTN}
					>
					<Text style={GLOBAL.TXTMAINBTN}>Login</Text>
				</TouchableOpacity>
			</View>
		</View>
		)
}

const mapStateToProps = (state) => ({
	
});

const mapDispatchToProps = (dispatch) => ({
	processLogin: (data) => dispatch({type: 'LOGIN', payload: data}),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);

const styles = StyleSheet.create({
	position: {
		position:'absolute',
		top: GLOBAL.HDIMENS * 0.2
	}
})