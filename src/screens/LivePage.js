import React from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity, Dimensions} from 'react-native';
import * as GLOBAL from '../constant'

export default function LivePage(props){
	return (
		<View style={GLOBAL.CONTAINER}>
			<View style={[GLOBAL.CONTENT, styles.position]}>
				<Text style={{alignSelf:'center', marginVertical:20, fontSize: 20}}>Live Page</Text>
			</View>
		</View>
		)
}

const styles = StyleSheet.create({
	position: {
		position:'absolute',
		top: GLOBAL.HDIMENS * 0.4
	}
})