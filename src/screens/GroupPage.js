import React, {useState} from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity, TextInput} from 'react-native';
import {connect} from 'react-redux';
import * as GLOBAL from '../constant'
import DropDownPicker from 'react-native-dropdown-picker';
import ImagePicker from 'react-native-image-picker';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/Ionicons';
import {useNavigation} from '@react-navigation/native';

const options = {
  title: 'Select Image',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};


function GroupPage(props){
	const [name, setName] = useState('KELAS XII IPA 4 - SIANG. 1')
	const [jenjang, setJenjang] = useState()
	const [image, setImage] = useState()
	const [rawImage, setRawImage] = useState()

	const navigation = useNavigation();

	function pickImage() {
	    ImagePicker.showImagePicker(options, (response) => {
	      

	      if (response.didCancel) {
	        console.log('User cancelled image picker');
	      } else if (response.error) {
	        console.log('ImagePicker Error: ', response.error);
	      } else if (response.customButton) {
	        console.log('User tapped custom button: ', response.customButton);
	      } else {
	        const source = {
	          uri: response.uri,
	          type: response.type,
	          name: response.fileName,
	          data: response.data,
	        };

	        setRawImage(source);
	        setImage(response.uri);
	        console.log(response.uri)
	      }
	    });
	  }

	  const submit = () => {
	  	const data = {
	  		kelas_id : jenjang,
	  		nama : name,
	  		thumbnail : image
	  	}
      props.addGroup(data);
      navigation.navigate('Diskusi')

  };

	return (
		<View style={GLOBAL.CONTAINER}>
			<View style={[GLOBAL.CONTENT, styles.position]}>
				{image == null ? (
					<TouchableOpacity
						 onPress={() => pickImage() }
						>
						<View style={styles.iconStyle}>
							<Icon name={'camera'} size={25} color={'#222'}/>
						</View>
						<Text style={styles.addImage}>Tambahkan Foto Group</Text>
					</TouchableOpacity>
					) :(
					<>
					<Image source={{uri : image}} style={{width:100,height:100,alignSelf:'center', marginVertical:50}}/>
					</>
					)}
				
				
				<TextInput
		          value={name}
		          placeholder='Nama Grup'
		          onChangeText={(text) => setName(text)}
		          style={[GLOBAL.FORMINPUT, styles.formInput]}
		        />
		     	<DropDownPicker
					    items={[
					        {label: '1', value: '1', hidden: true},
					        {label: '2', value: '2'},
					        {label: '3', value: '3'},
					    ]}
					    
					    containerStyle={{height: 40, width:GLOBAL.WDIMENS * 0.5, marginVertical:5}}
					    style={{backgroundColor: '#ddd'}}
					    itemStyle={{
					        justifyContent: 'flex-start'
					    }}
					    labelStyle={{
					    	color : '#222'
					    	}}
					    placeholder='Jenjang'
					    dropDownStyle={{backgroundColor: '#ddd'}}
					    onChangeItem={item => setJenjang(item.value)}
					/>

				<TouchableOpacity
					onPress={() => submit()}
					style={[GLOBAL.MAINBTN, styles.button]}
					>
					<Text style={GLOBAL.TXTMAINBTN}>Buat Grup</Text>
				</TouchableOpacity>
			</View>
		</View>
		)
}

const mapStateToProps = (state) => ({
	
});

const mapDispatchToProps = (dispatch) => ({
	addGroup: (data) => dispatch({type: 'ADD_GROUP', payload: data}),
});

export default connect(mapStateToProps, mapDispatchToProps)(GroupPage);

const styles = StyleSheet.create({
	position: {
		// position:'absolute',
		// top: GLOBAL.HDIMENS * 0.1
	},
	formInput:{
		backgroundColor:'#ddd',
		color:'#222'
	},
	button:{
		marginTop:100
	},
	iconStyle:{
		alignItems:'center', 
		marginTop:50,
		backgroundColor:'#ddd', 
		width: 100, 
		height: 100,
		 justifyContent:'center', 
		 alignSelf:'center',
		 borderRadius:10
	},
	addImage:{alignSelf:'center', 
	marginVertical:20, 
	fontSize: 15, 
	color: '#777'}
})