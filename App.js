/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import {Provider} from 'react-redux';
import store from './src/redux/store';
import AppStack from './src/navigator/AppStack';

const App: () => React$Node = () => {
  return (
    <Provider store={store}>
      <AppStack />
    </Provider>
  );
};


export default App;
